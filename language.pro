TEMPLATE = app
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    builtins.cpp

INCLUDEPATH += $$PWD/3rdparty

LIBS += -L$$PWD/3rdparty -lwfile \
        -lboost_filesystem		 \
        -lboost_system

HEADERS += \
    builtins.h

