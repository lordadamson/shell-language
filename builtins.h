#ifndef BUILTINS_H
#define BUILTINS_H

#include <string>
#include <vector>
#include <map>

extern const
std::map<std::string, bool(*)(const std::vector<std::string>&)>
builtins;

#endif // BUILTINS_H
