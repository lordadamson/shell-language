//rdi_wfileio.h

#ifndef WFILE_H
#define WFILE_H

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
#define WINDOWS_OS
#else
#define LINUX_OS
#endif

#ifdef WINDOWS_OS
#include "windows_dirent.h"
#else
#include <dirent.h>
#endif

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iostream>

extern "C++"
{
namespace RDI
	{

	std::wstring read_wfile(const std::string &filename);
	std::vector<std::wstring> read_wfile_lines(const std::string &filename);
	std::string read_file(const std::string& filename);
	std::vector<std::string> read_file_lines(const std::string& filename);

	bool write_wfile(const std::string &filename,
					 const std::wstring &fileContent);

	bool append_to_wfile(const std::string &filename,
						 const std::wstring &content);

	bool write_wfile_lines(const std::string& filename,
						   const std::vector<std::wstring>& linesToWrite);

	bool write_file(const std::string& filename,
					const std::string& fileContent);

	bool append_to_file(const std::string& filename,
						const std::string& content);

	bool write_file_lines(const std::string& filename,
						  const std::vector<std::string>& linesToWrite);

	template<typename T, template <typename...> class Map,
			 typename K, typename V>
	bool write_map(const std::string& filename,const Map<K,V>& mapToWrite)
	{
		std::basic_ofstream<T> of(filename.c_str());
		if(!of.is_open())
			return false;

		for (std::pair<K,V> line : mapToWrite)
			of << line.first << " "<< line.second << std::endl;

		return true;
	}

	template<typename T, template <typename...> class Map,
			 typename K, typename V>
	bool write_map(const std::string& filename,
	               const Map<K,std::vector<V> >& mapToWrite)
	{
		std::basic_ofstream<T> of(filename.c_str());
		if(!of.is_open())
			return false;

		for (std::pair<K,std::vector<V> > line : mapToWrite)
		{
			of << line.first << std::endl;
			for(V value : line.second )
				of << value << std::endl;
		}
		return true;
	}

	bool delete_file(const std::string& path);
	std::string get_absolute_path(std::string path);

	// returns the location of the binary executable ex: "/home/rdi/bin"
	std::string get_current_directory();

	std::vector<std::string> get_directory_content(const std::string& path);
	bool create_directory(std::string path);
	bool delete_directory(const std::string& path);

	bool dump_matrix(const std::string &file_name,
	                 std::vector<std::vector<float> > &input_matrix);

	bool file_exists(const std::string &file_name);
	bool is_directory(const std::string& path);
	std::string extract_path_from_filename(const std::string& filename);
	std::string extract_filename(const std::string& path);
	std::string extract_filename_without_extension(const std::string& path);
	std::string extract_extention_from_path(const std::string& filename);

	} // namespace RDI
}

#endif // RDI_WFILEIO_H

