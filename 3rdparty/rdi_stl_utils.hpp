#ifndef RDI_STL_UTILS_H
#define RDI_STL_UTILS_H

#include <algorithm>
#include <cmath>
#include <chrono>
#include <condition_variable>
#include <optional>
#include <iostream>
#include <memory>
#include <mutex>
#include <queue>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <type_traits>

/// Use this macro to create a smart pointer to an array
#define MAKE_UNIQUE_ARRAY(TYPE, NAME, SIZE)                                    \
	std::unique_ptr<TYPE[]> NAME = std::make_unique<TYPE[]>((SIZE))

/// Use this macro when you own an array ptr that you did not create
/// but since you own it, you're responsible of freeing it.
/// Note that the array must be created using the 'new' keyword.
/// malloc may not produce compile error and the program may run normally
/// but it can (and will) silently leak memory in the background
#define MAKE_UNIQUE_ARRAY_PTR(TYPE, NAME, ARRAY)                               \
	std::unique_ptr<TYPE, void (*)(TYPE*)> NAME                                \
	= std::unique_ptr<TYPE, void (*)(TYPE*)>((ARRAY),						   \
	unique_array_ptr_deleter)

extern "C++" {

namespace RDI
{

template<typename Container, typename ElementType>
typename Container::const_iterator
find(const Container& c, const ElementType& e)
{
	return std::find(std::begin(c), std::end(c), e);
}

template<typename Container, typename Functor>
typename Container::const_iterator
find_if(const Container& c, const Functor& f)
{
	return std::find_if(std::begin(c), std::end(c), f);
}

template <typename T>
void
replace(std::basic_string<T>& str, T old_value, T new_value)
{
	std::replace(str.begin(), str.end(), old_value, new_value);
}

inline namespace logging
{
	template <typename T, typename = void>
	struct is_iterable : std::false_type
	{
	};
	template <typename T>
	struct is_iterable<T,
			std::void_t<decltype(std::declval<T>().begin()),
			decltype(std::declval<T>().end())>>
												: std::true_type
	{
	};

	template <typename T>
	void print(T t);

	template <typename T>
	void print_impl(T t)
	{
		std::cout << "[ ";
		for (auto i : t)
		{
			print(i);
		}
		std::cout << "] ";
	}

	template <typename T>
	void print(T t)
	{
		if constexpr (is_iterable<T>()
					  && not std::is_convertible<T, std::string>::value)
		{
			print_impl(t);
		}
		else
		{

			if constexpr (std::is_convertible<T, std::wstring>::value or
						  std::is_same_v<T,wchar_t>)
					std::wcout << t << ' ';
			else
			std::cout << t << ' ';
		}
	}

	template <typename T, typename... Args>
	std::ostream& print(T first, Args... args)
	{
		print(first);
		std::cout << std::endl;
		print(args...);
		if(sizeof... (args) == 1)
		{
			std::cout << std::endl;
		}
		return std::cout;
	}
} // namespace logging



template <typename T, size_t N>
constexpr inline auto length_of( T(&)[N] )
{
	return N;
}

template <typename T>
using vec = std::vector<T>;

/// send a char and it will return a string containing
/// that char
/// In case of a null char it returns an empty string.
///
/// All char types supported. char, wchar_t, char_16t, char_32t
template <typename T>
std::basic_string<T> char_to_string(T c)
{
	if (c == '\0')
	{
		return std::basic_string<T>();
	}

	return std::basic_string<T>(1, c);
}

template<typename T>
std::basic_string<T>
collapse(const std::basic_string<T>& bk_line, T collapsee)
{
	std::basic_string<T> output;
	bool first = true;

	for(const T& c : bk_line)
	{
		if(c == collapsee && first)
		{
			first = false;
			output += c;
			continue;
		}
		else if(c == collapsee)
		{
			continue;
		}

		first = true;
		output += c;
	}

	return output;
}

template<typename T>
std::basic_string<T>
collapse(const T* bk_line, T collapsee)
{
	return collapse(std::basic_string<T>(bk_line), collapsee);
}

template <typename T>
std::vector<std::basic_string<T>> split(const std::basic_string<T>& input,
										const T delimiter = ' ')
{
	std::basic_string<T> buff;
	std::vector<std::basic_string<T>> output;

	for (const auto& c : input)
	{
		if (c != delimiter)
		{
			buff += c;
		}
		else
		{
			if (c == delimiter && !buff.empty())
			{
				output.push_back(buff);
				buff.clear();
			}
		}
	}

	if (!buff.empty())
	{
		output.push_back(buff);
	}

	return output;
}

template <typename T>
std::vector<std::basic_string<T>>
split(const std::basic_string<T>& input,
	  const std::basic_string<typename std::remove_const<T>::type>&
	  delimiters)
{
	std::basic_string<T> buff;
	std::vector<std::basic_string<T>> output;
	std::unordered_set<T> delimiter_set;
	for (auto d : delimiters)
	{
		delimiter_set.insert(d);
	}

	for (const auto& c : input)
	{
		if (delimiter_set.find(c) == delimiter_set.end())
		{
			buff += c;
		}
		else
		{
			if (delimiter_set.find(c) != delimiter_set.end()
				&& !buff.empty())
			{
				output.push_back(buff);
				buff.clear();
			}
		}
	}

	if (!buff.empty())
	{
		output.push_back(buff);
	}

	return output;
}

template <typename T>
std::vector<std::basic_string<T>>
split_even_empty_buffers(const std::basic_string<T>& input,
						 const std::basic_string<typename std::remove_const<T>::type>&
						 delimiter)
{
	std::basic_string<T> buff;
	std::vector<std::basic_string<T>> output;

	for ( uint i = 0 ; i < input.size() ; i++ )
	{
		if ( i+delimiter.size() <= input.size() && input.substr(i,delimiter.size()) == delimiter )
		{
			output.push_back(buff);
			buff.clear();
			i += delimiter.size()-1;
		}
		else
		{
			buff += input[i];
		}
	}

	output.push_back(buff);

	return output;
}

template <typename T>
void unique_array_ptr_deleter(T* object)
{
	delete[] object;
}

template <typename T>
std::vector<T> concat_vectors(std::vector<T> a, std::vector<T> b)
{
	a.insert(a.end(), b.begin(), b.end());
	return a;
}

template <typename T>
std::vector<T> concat_vectors(const std::vector<std::vector<T>>& vecs)
{
	std::vector<T> output;

	for (size_t i = 0; i < vecs.size(); ++i)
	{
		output = RDI::concat_vectors(output, vecs[i]);
	}

	return output;
}

template <typename T>
T remove_spaces(const T& input)
{
	T result = "";
	for (const auto& i : input)
	{
		if (i != ' ')
		{
			result += i;
		}
	}
	return result;
}

inline bool
fuzzy_compare(double p1, double p2)
{
	return std::fabs(p1 - p2) <=
			0.000000000001 * std::min(std::abs(p1), std::abs(p2));
}

inline bool
fuzzy_compare(float p1, float p2)
{
	return (std::fabs(p1 - p2) <=
			0.00001f * std::min(std::abs(p1), std::abs(p2)));
}


template <typename Container,
		  typename Element,
		  typename Functor>
std::vector<size_t>
find_all_occurrences(Container c, Element to_find, Functor comparer)
{
	std::vector<size_t> occurrences;
	typename Container::const_iterator it;
	size_t i = 0;
	for(it = c.begin(); it != c.end(); it++, i++)
	{
		if(comparer(*it, to_find))
		{
			occurrences.push_back(i);
		}
	}

	return occurrences;
}

/// @brief Our way of doing python inspired list comprehension. Or in our
/// case, vector comprehension.
/// @param stuff list of stuff to be processed
/// @param func a process that is performed on each element in stuff
/// @returns The processed stuff
///
/// ## Example Usage
/// convert a list of ints to list of strings
///
/// ```
/// vector<string>
/// convert_to_strings(const vector<int>& integers)
/// {
///		return create_vector_from<vector<string>>(integers, std::to_string);
/// }
/// ```
template<typename R, typename T, typename Functor>
R create_vector_from(std::vector<T> stuff, Functor func)
{
	R output;
	output.reserve(stuff.size());

	for(const T& element : stuff)
	{
		output.push_back(
					func(element)
					);
	}

	return output;
}

/// @brief Finds all occurrences of a certain element in a container.
/// Could be any stl container or stl complaint containers (defines .size()
/// const_iterator, .begin() and .end())
/// @param c any container that can have its elements accessed with indices,
/// like std::vector
/// @param to_find the element to find the occurrences of.
/// @return the indices of the to_find in c
template <typename Container, typename Element>
std::vector<size_t>
find_all_occurrences(Container c, Element to_find)
{
	if constexpr(std::is_floating_point_v<Element>)
	{
		return find_all_occurrences(c, to_find, [](Element e1, Element e2){
			return fuzzy_compare(e1, e2);
		});
	}

	return find_all_occurrences(c, to_find, [](Element e1, Element e2){
		return e1 == e2;
	});
}

template <typename T, typename Container>
bool within_container(const T& element, const Container& v)
{
	return find(v, element) != v.end();
}

/// T is expected to be a multipliable type. (int, float, double... etc.)
template <class T>
std::vector<T> multiply_by_constant(const std::vector<T>& vec, T constant)
{
	std::vector<T> output(vec);

	for (auto& i : output)
	{
		i *= constant;
	}

	return output;
}

/// T is expected to be a sumable type. (int, float, double... etc.)
template <class T>
std::vector<T> sum_vectors(const std::vector<T>& vec1,
						   const std::vector<T>& vec2)
{
	if (vec1.size() != vec2.size())
	{
		throw std::runtime_error("vectors are not of the same size");
	}

	std::vector<T> output(vec1.size());
	for (size_t i = 0; i < vec1.size(); i++)
	{
		output[i] = vec1[i] + vec2[i];
	}

	return output;
}

/// T is expected to be a sumable type. (int, float, double... etc.)
template <class... Args, class T>
std::vector<T> sum_vectors(const std::vector<T>& vec1,
						   const std::vector<T>& vec2, Args... args)
{
	std::vector<T> output = sum_vectors(vec1, vec2);
	return sum_vectors(output, args...);
}

template <typename T>
bool has_suffix(const std::basic_string<T>& str,
				const std::basic_string<T>& suffix)
{
	if (suffix.size() > str.size())
		return false;
	return std::equal(suffix.rbegin(), suffix.rend(), str.rbegin());
}

template <typename T>
bool has_suffix(const std::basic_string<T>& str, const T suffix[])
{
	return has_suffix(str, std::basic_string<T>(suffix));
}

template <typename T>
std::basic_string<T>
remove_characters_from_the_end(const std::basic_string<T>& str,
							   size_t number_of_characters_to_remove)
{
	size_t lastindex = str.size() - number_of_characters_to_remove;

	if (lastindex > str.size())
	{
		throw std::runtime_error("Trying to remove "
								 + std::to_string(lastindex)
								 + " characters from a "
								 + std::to_string(str.size())
								 + " long string.");
	}
	return str.substr(0, lastindex);
}

inline std::wstring to_wstring(const std::string& str)
{
	return std::wstring(str.begin(), str.end());
}

template <typename T>
std::basic_string<T> remove_suffix(const std::basic_string<T>& str,
								   const std::basic_string<T>& suffix)
{
	if (!has_suffix(str, suffix))
	{
		throw std::runtime_error(
					"String: " + str + " does not have the suffix: " + suffix);
	}
	return remove_characters_from_the_end(str, suffix.size());
}

/// pass any std container and the desired size of chunk
/// and this function will return an std::vector of that container
/// where each container within is of size size_of_chunk
/// except for the last one which could be less (because it's the remainder)
template <typename Container>
std::vector<Container> split_to_chunks(const Container& c,
									   size_t size_of_chunk)
{
	std::vector<Container> chunks;
	size_t index = 0;

	typename Container::const_iterator start = c.begin();

	while (start != c.end())
	{
		size_t stride = (index + size_of_chunk) > c.size()
				? c.size() - index
				: size_of_chunk;

		typename Container::const_iterator end = start;
		std::advance(end, stride);

		index += stride;
		Container tmp(start, end);

		start = end;
		chunks.push_back(tmp);
	}

	return chunks;
}

/// pass any std container and the desired number of chunk
/// and this function will return an std::vector of that container
/// of size num_of_chunks.
template <typename Container>
std::vector<Container> split_to_x_chunks(const Container& c,
										 size_t num_of_chunks)
{

	if (num_of_chunks <= 0)
	{
		throw std::overflow_error(
					"num_of_chunks can't be less than or equal to zero\n");
	}
	std::vector<Container> out;
	int divide = c.size() / num_of_chunks;
	int remainder = c.size() % num_of_chunks;
	auto begin = std::begin(c);
	auto end = std::begin(c);

	for (size_t i = 0; i < num_of_chunks; ++i)
	{
		if (i == 0)
		{
			std::advance(end, divide + (remainder > 0));
		}

		out.emplace_back(begin, end);
		std::advance(begin, divide + (remainder > 0));
		// Do not advance to the beginning of the container when you reach
		// the end
		end = (i == num_of_chunks - 1)
				? std::end(c)
				: std::next(end, divide + (remainder > 1));

		remainder--;
	}

	return out;
}

template <typename Container, typename Type>
int index_of_element(Type element, Container c)
{
	auto it = find(c, element);

	return it == c.end() ? -1 : std::distance(c.cbegin(), it);
}

template<typename Real>
std::vector<Real> string_to_vec_real(const std::vector<std::string>& str)
{
	std::vector<Real> output(str.size());

	for (size_t i = 0; i < output.size(); i++)
	{
		output[i] = stof(str[i]);
	}

	return output;
}

template<typename Real>
std::vector<Real> string_to_vec_real(const std::string& str)
{
	std::vector<std::string> tokens = split(str);
	std::vector<Real> output(tokens.size());

	for (size_t i = 0; i < output.size(); i++)
	{
		output[i] = stof(tokens[i]);
	}

	return output;
}

/// Condition could be anything that overloads the ! operator
/// so it could be a boolean, raw ptr, shared_ptr, optional... etc.
template<typename T, typename E>
T throw_unless(T&& condition, E&& exception)
{
	if(!condition)
	{
		throw exception;
	}

	return condition;
}

/// @brief static casts a singed integral type to and unsigned one.
/// Used to avoid warnings of implicit conversion.
/// More details here: https://stackoverflow.com/questions/53545143/comparison-between-signed-and-unsigned-is-static-cast-the-only-solution
/// @param an int that you want to convert to unsigned
/// @returns unsigned of whatever you passed in
template<class T>
typename std::make_unsigned<T>::type
ucast(T a)
{
	return static_cast<typename std::make_unsigned<T>::type>(a);
}

class Timer
{
public:
	Timer() {}
	~Timer() {}

	void start_timer()
	{
		this->start = std::chrono::high_resolution_clock::now();
	}

	float get_elapsed_time()
	{
		using namespace std::chrono;

		this->end = high_resolution_clock::now();
		auto duration
				= duration_cast<milliseconds>(this->end - this->start).count();
		float output = float(duration) / 1000.0f;
		return output;
	}

#ifndef __UNIT_TESTING__
private:
#endif
	std::chrono::high_resolution_clock::time_point start;
	std::chrono::high_resolution_clock::time_point end;
};

// A threadsafe-queue.
template <class T>
class SafeQueue
{
public:
	SafeQueue(void) : q(), m(), c() {}

	~SafeQueue(void) {}

	// Add an element to the queue.
	void enqueue(T t)
	{
		std::lock_guard<std::mutex> lock(m);
		q.push(t);
		c.notify_one();
	}

	// Get the "front"-element.
	// If the queue is empty, wait till a element is avaiable.
	T dequeue(void)
	{
		std::unique_lock<std::mutex> lock(m);
		while (q.empty())
		{
			// release lock as long as the wait and reaquire it afterwards.
			c.wait(lock);
		}
		T val = q.front();
		q.pop();
		return val;
	}
	size_t size() { return q.size(); }

private:
	std::queue<T> q;
	mutable std::mutex m;
	std::condition_variable c;
};

/// OT => Optional Tuples
template <typename... Args>
using OT = std::optional<std::tuple<Args...>>;

/// SQO => SafeQueue of Optionals
template <typename T>
using SQO = SafeQueue<std::optional<T>>;

/// SQOT => SafeQueue of Optional Tuples
template <typename... Args>
using SQOT = SafeQueue<OT<Args...>>;

/// OV => Optional Vectors
template <typename T>
using OV = std::optional<std::vector<T>>;

/// SQOV => SafeQueue of Optional Vectors
template <typename T>
using SQOV = SafeQueue<OV<T>>;

} // namespace RDI

} // extern "C++"

#endif // RDI_STL_UTILS_H
