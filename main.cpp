#include <iostream>
#include <string>

#include <spawn.h>
#include <sys/wait.h>
#include <string.h>

#include <rdi_stl_utils.hpp>
#include <rdi_wfile.hpp>

#include "builtins.h"

using namespace std;

extern char **environ;

vector<string>
handle_quotations(const vector<string>& tokens)
{
	vector<string> output;

	string current_token;
	bool within_token = false;

	for(string token : tokens)
	{
		if(token.front() == '\"')
		{
			token = string(token.begin() + 1, token.end()); // removes the "
			within_token = true;
		}

		if(token.back() == '\"')
		{
			token.pop_back();
			within_token = false;
		}

		if(within_token)
		{
			token += " ";
		}

		current_token += token;

		if(not within_token)
		{
			output.push_back(current_token);
			current_token.clear();
		}
	}

	return output;
}

vector<string>
parse(const string& line)
{
	string buff;
	vector<string> output;
	char delimiter = ' ';
	bool within_qoutes = false;

	for (const auto& c : line)
	{
		if(c == '\"')
		{
			within_qoutes = true;
			continue;
		}

		if(within_qoutes)
		{
			if(c == '\"')
			{
				within_qoutes = false;
				output.push_back(buff);
				continue;
			}

			buff += c;
			continue;
		}

		if (c != delimiter)
		{
			buff += c;
		}
		else
		{
			if (c == delimiter && !buff.empty())
			{
				output.push_back(buff);
				buff.clear();
			}
		}
	}

	if (!buff.empty())
	{
		output.push_back(buff);
	}

	return output;
}

string
extract_command(const vector<string>& tokens)
{
	return tokens[0];
}

char**
extract_argv(const vector<string>& tokens)
{
	if(tokens.empty())
		return nullptr;

	char** arg_v = new char*[tokens.size()+1];

	for(size_t i = 0; i < tokens.size(); i++)
	{
		arg_v[i] = const_cast<char*>(tokens[i].c_str());
	}

	arg_v[tokens.size()] = nullptr;

	return arg_v;
}

vector<string>
extract_arguments(const vector<string>& tokens)
{
	if(tokens.size() < 2)
	{
		return {};
	}

	return vector<string>(tokens.begin() + 1, tokens.end());
}

string
find_command(string command)
{
	vector<string> paths = RDI::split(string(getenv("PATH")), ':');

	for(const string& path : paths)
	{
		vector<string> available_commands = RDI::get_directory_content(path);

		if(RDI::find(available_commands, command) != available_commands.end())
		{
			return path + "/" + command;
		}
	}

	return "";
}

bool
builtin_exists(const string& command)
{
	auto it = builtins.find(command);

	return it != builtins.end();
}

bool
execute_builtin(const string& command,
				const vector<string>& argments)
{
	if(not builtin_exists(command))
	{
		return false;
	}

	builtins.at(command)(argments);
	return true;
}

int main()
{
	string line;
	string prompt = "$ ";
	cout << prompt;
	while(getline(cin, line))
	{
		vector<string> tokens = parse(line);

		if(tokens.empty())
		{
			continue;
		}

		string command = extract_command(tokens);
		string command_path = find_command(command);

		if(command_path.empty() and
		   !execute_builtin(command, extract_arguments(tokens)))
		{
			cerr << command << ": Command not found\n";
		}

		// we need to free those
		char** arg_v = extract_argv(tokens);

		pid_t pid, status;
		status = posix_spawn(&pid, command_path.c_str(),
							 nullptr, nullptr, arg_v, environ);
		waitpid(pid, &status, 0);

		delete[] arg_v;

		cout << prompt;
	}
}
