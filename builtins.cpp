#include "builtins.h"
#include <unistd.h>

using namespace std;

bool
_exit_(const vector<string>&)
{
	exit(0);
}

bool
cd(const vector<string>& args)
{
	if(args.size() < 1)
	{
		return false;
	}

	return chdir(args[0].c_str());
}

bool
pwd(const vector<string>&)
{
	return true;
}

const map<string, bool(*)(const vector<string>&)> builtins =
{
	{"exit", _exit_},
	{"cd", cd},
	{"pwd", pwd}
};
